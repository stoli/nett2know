const processor = require("asciidoctor")();

const options = {
  // "unsafe" must be used, otherwise "attributes.adoc" will not be read and
  // even if the attributes are added manually in the KnowHow file, "font-awesome.css" and "highlight/styles/xcode.min.css" will not be included.
  safe: "unsafe",
};

processor.convertFile(process.argv[2], options);
